<?php

/**
 * Description of setup
 *
 * @author jakschu
 */
//class setup {
//put your code here
// simple setup for the image gallery

function setupGallery($srcPath) {
	$origRes = imagecreatefromjpeg($srcPath);
	$origSha1 = sha1_file($srcPath);
	$origInfo = getimagesize($srcPath);
	$w = $origInfo[0];
	$h = $origInfo[1];
	$thumbSize = 400;
	if ($w > $h ? true : false) { // landscape
		$w2 = $w * ($h / $w);
		$h2 = $thumbSize;
		$x = ceil(($w - $h) / 2);
		$y = 0;
	} else { // portrait
		$w2 = $thumbSize;
		$h2 = $h * ($w / $h);
		$x = 0;
		$y = ceil(($h - $w) / 2);
	}
	//echo "<p>w:".$w." h:".$h." to w:".$w2." h:".$h2."</p>";
	$thumb = imagecreatetruecolor($thumbSize, $thumbSize);
	imagecopyresampled($thumb, $origRes, 0, 0, $x, $y, $w2, $h2, $w, $h);
	//imagewebp($thumb, "thumb/".$origSha1 . ".webp");
	imagejpeg($thumb, "thumb/".$origSha1 . ".jpg");
	imagedestroy($thumb);
	rename($srcPath, "img/".$origSha1.".jpg");
}

$images = array_slice(scandir('orig/'), 2);
foreach ($images as $img) {
	setupGallery("orig/".$img);
}
		
//}
