<?php
// default config
return array(
		"thumbnails" => [
				"size" => 400, // in px
				"folder" => "thumb"
		],
		"images" => [
				"sha1" => true
		]
);