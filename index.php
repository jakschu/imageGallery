<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Bildgallery</title>
		<link href="css/main.css" rel="stylesheet" type="text/css" >
		<link href="css/print.css" rel="stylesheet" type="text/css" media="print">
	</head>
	<body>
		<script>
			function lightBox(el){
				alert("Open lightbox stub\nTODO: Implement lightbox!");
				console.log(el);
				console.log(el.src);
				var fullimg = el.src.replace("thumb","img");
				console.log(fullimg);
				window.location.href = fullimg;
				
			}
		</script>
		<div class="container">
			<h1 id="title">Willkommen zur Bildergallery</h1>
			<div class="photos">
				<?php
				$images = array_slice(scandir('thumb/'), 2);
				foreach ($images as $img) {
					printf("<img src=thumb/%s onclick=\"lightBox(this);\">", $img);
				}
				?>
			</div>
		</div>
	</body>
</html>
